import { Component, JSX, Setter } from 'solid-js'
import { IBook } from '../../assets/booksDB'

interface IAddBook {
  add: Setter<IBook[]>
}

export const AddBook: Component<IAddBook> = ({ add }) => {
	
  return (
    <form id='addBookForm'>
      <fieldset>
        <label><input name='book-title' type='text' />Book Title</label>
        <label><input name='book-author' type='text' />Author</label>
      </fieldset>
      <button type='submit' onSubmit={e => e.preventDefault()} onClick={addNewBook}>Add Book</button>
    </form>
  )

  /**
	 * JSX.EventHandler<HTMLButtonElement, MouseEvent>
	 */
  function addNewBook (e: MouseEvent & {
    currentTarget: HTMLButtonElement;
    target: Element;
	}): void {
    e.preventDefault()
    add(books => [...books, newBook()])
  }

  // document.forms.addBookForm.elements['book-author'].value
	interface IAddBookForm extends HTMLCollectionOf<HTMLFormElement> {
		addBookForm: HTMLFormElement
	}
	interface IAddBookFormElemnts extends HTMLFormControlsCollection {
		'book-author': HTMLFormElement
		'book-title': HTMLFormElement
	}

  function newBook (): IBook {
    const addBookForm = (document.forms as IAddBookForm).addBookForm
		const addBookFormElemnts = addBookForm.elements as IAddBookFormElemnts

		const newBook = {
      author: addBookFormElemnts['book-author'].value,
      title: addBookFormElemnts['book-title'].value,
      imgsrc: ''
    }

    addBookForm.reset()
    return newBook
  }
}
